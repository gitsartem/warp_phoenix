# This file is responsible for configuring your application
# and its dependencies with the aid of the Mix.Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

# Configures the endpoint
config :warp_phoenix, WarpPhoenixWeb.Endpoint,
  url: [host: "localhost"],
  secret_key_base: "W7nGfdvOcwcF7RyC1LEcGlgHbYAB7Ys8HAsLtyo1zNbCCQOpqN6jFajkg/iMSTK2",
  render_errors: [view: WarpPhoenixWeb.ErrorView, accepts: ~w(json), layout: false],
  pubsub_server: WarpPhoenix.PubSub,
  live_view: [signing_salt: "ZfAN98Tw"]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{Mix.env()}.exs"
